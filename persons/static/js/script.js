$(document).ready(function () {

    function getCookie(name) {
        var cookieValue = null;
        if (document.cookie && document.cookie !== '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) === (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }

    var csrftoken = getCookie('csrftoken');


    $('#auto_fixture_form').on('submit', function (event) {
        event.preventDefault();
        var form = $('#id_how_many_persons').val();
        $('#loading-indicator').show();
        $.ajax({

            url: autofixtures_url,
            processData: true,
            type: 'POST',
            data: {
                how_many_persons: form,
                csrfmiddlewaretoken: csrftoken,
            },
            success: function (data) {
                $('body').html(data);
                $('#loading-indicator').hide();
            }

        });
    });


    $("#birthday_search, #study_from_search, #study_to_search," +
        " #extradition_date, #birthday_date, #period_of_study_from, #period_of_study_to").datepicker({
        dateFormat: 'yyyy-mm-dd',
        todayButton: new Date()
    })
});
