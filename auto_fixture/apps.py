from django.apps import AppConfig


class AutoFixtureConfig(AppConfig):
    name = 'auto_fixture'
