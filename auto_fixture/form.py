from django import forms

class AutoFixtureForm(forms.Form):
    how_many_persons = forms.IntegerField(widget=forms.TextInput(
        attrs={'type': "text", 'class': "form-control", 'placeholder': "Количество персон",
               'aria-describedby': "basic-addon1"}))
    # max_docs = forms.IntegerField(widget=forms.TextInput(
    #     attrs={'type': "text", 'class': "form-control", 'placeholder': "Макс. кол-во документов на персону",
    #            'aria-describedby': "basic-addon2"}))
