import os, random, datetime, uuid

from django.shortcuts import render, HttpResponse
from django.template.loader import render_to_string
from django.core.files import File
from persons.settings import STATICFILES_DIRS

from viewer.models import Person, PersonsDocs
from .form import AutoFixtureForm

# Create your views here.

"""
PERSON GENERATOR FUNCTIONS
"""
def get_fio(sex):
    """
     Simple FIO generator
     :param: sex - male or female?
     :return: FIO based on sex
    """

    first_name_m = [
        'Александр', 'Максим', 'Иван', 'Артем', 'Дмитрий', 'Никита', 'Михаил', 'Даниил', 'Егор', 'Андрей'
    ]

    first_name_f = [
        'Анастасия', 'Мария', 'Дарья', 'Анна', 'Елизавета', 'Полина', 'Виктория', 'Екатерина', 'Софья', 'Александра'
    ]

    surename = [
        'Лебедев', 'Козлов', 'Новиков', 'Морозов', 'Петров', 'Волков', 'Соловьёв', 'Васильев', 'Зайцев', 'Павлов',
        'Семёнов', 'Голубев', 'Виноградов', 'Богданов', 'Воробьёв'
    ]

    third_name_m = [
        'Игоревич', 'Игнатиевич', 'Гордеевич', 'Давидович', 'Герасимович', 'Федотович', 'Эдуардович', 'Харитонович'
    ]

    third_name_f = [
        'Гордеевна', 'Горациевна', 'Далматовна', 'Диомидовна', 'Евграфиевна', 'Евлогиевна', 'Харламповна', 'Яковлевна'
    ]

    if sex == 'm':
        return '{} {} {}'.format(random.choice(surename), random.choice(first_name_m), random.choice(third_name_m))
    else:
        return '{}а {} {}'.format(random.choice(surename), random.choice(first_name_f), random.choice(third_name_f))


def get_phone_number():
    """
    Simple phone number generator
    :return: phone number, format +79051234455
    """
    phone_number_codes = ['900', '902', '903', '904', '905', '906', '908', '909', '950', '951', '953']
    return '+7{}{}'.format(random.choice(phone_number_codes), random.randint(1000000, 9000000))


def get_birthday_date():
    """
    Birthday generator! Whoooo ) it's a time machine!
    :return: date
    """
    year = random.randint(1950, 2000)
    month = random.randint(1, 12)
    day = random.randint(1, 28)
    return datetime.date(year, month, day)


def get_period_of_study(year):
    """
    period of study generator!
    :param: year of a birthday
    :return: tuple of dates
    """
    new_year = year + random.randint(7, 15)
    month = random.randint(1, 12)
    day = random.randint(1, 28)
    return (datetime.date(new_year, month, day), datetime.date(new_year + random.randint(3, 7), month, day))


def get_group_of_study():
    """
    Get random from a list
    :return: one of list elements
    """
    group_of_study = ['очная', 'заочная', 'вечерняя', 'дневная']
    return random.choice(group_of_study)


def get_edu_organization():
    """
    Get random from a list
    :return: one of list elements
    """
    edu_organization = [
        'Аничков лицей', 'Гимназия 171', 'ГБОУ Президентский ФМЛ № 239', 'СОШ 222 немецкого языка Петришуле',
        'Гимназия № 56', 'СОШ № 332'
    ]
    return random.choice(edu_organization)


"""
DOCS GENERATOR FUNCTIONS
"""

def get_doc_number():
    """
    UUID generator. Uses for doc number.
    :return: UUID
    """
    return uuid.uuid4()


def get_extradition_date(birthday_year):
    """
    Doc extradition date, based on birthday
    :param birthday_year: get a birthday
    :return: date
    """
    new_year = birthday_year + random.randint(18, 25)
    month = random.randint(1, 12)
    day = random.randint(1, 28)
    return datetime.date(new_year, month, day)

def get_doc_type():
    """
    Doc type getter, random from a list
    :return: a list item
    """
    doc_type = ['паспорт', 'студенческий', 'св. о рождении', 'права']
    return random.choice(doc_type)


def populate_model(how_many_persons):
    """
    Function for models (Person, PersonsDocs) population
    :param:
    how_many_persons - How many persons do we need?
    :return: create an instance of DB
    """
    sex_choices = ['муж', 'жен']

    i = 0
    while i != how_many_persons:
        birthday = get_birthday_date()
        mobile_number = get_phone_number()
        period_of_study_from, period_of_study_to = get_period_of_study(birthday.year)
        group = get_group_of_study()
        edu_organization = get_edu_organization()
        if random.choice(sex_choices) == 'муж':
            p = Person(fio=get_fio('m'), birthday_date=birthday, sex='муж', mobile_number=mobile_number,
                       period_of_study_from=period_of_study_from, period_of_study_to=period_of_study_to, group=group,
                       edu_organization=edu_organization)
            p.save()
            pd = PersonsDocs(number=get_doc_number(), extradition_date=get_extradition_date(birthday.year),
                             doc_type=get_doc_type(), person=p)
            file_name = os.path.join('img', 'm_scan.jpg')
            file_path = os.path.join(STATICFILES_DIRS[0], file_name)
            with open(file_path, 'rb') as doc_file:
                pd.doc_scan.save('doc scan', File(doc_file), save=True)
            pd.save()
            i += 1
        else:
            p = Person(fio=get_fio('f'), birthday_date=birthday, sex='жен', mobile_number=mobile_number,
                       period_of_study_from=period_of_study_from, period_of_study_to=period_of_study_to, group=group,
                       edu_organization=edu_organization)
            p.save()
            pd = PersonsDocs(number=get_doc_number(), extradition_date=get_extradition_date(birthday.year),
                             doc_type=get_doc_type(), person=p)
            file_name = os.path.join('img', 'f_scan.jpg')
            file_path = os.path.join(STATICFILES_DIRS[0], file_name)
            with open(file_path, 'rb') as doc_file:
                pd.doc_scan.save('doc scan.jpg', File(doc_file), save=True)
            pd.save()
            i += 1


def autofixtures(request):
    """
    View, render form depends on request type.
    :param request:
    :return: if is ajax: HttpResponse, else: render
    """
    if request.is_ajax():
        form = AutoFixtureForm(request.POST)
        if form.is_valid():
            populate_model(int(form.cleaned_data['how_many_persons']))
            html = render_to_string('auto_fixture/auto_fixture.html', {'form': AutoFixtureForm, 'msg': 'Наполнение базы завершено'})
            return HttpResponse(html)
    else:
        return render(request, 'auto_fixture/auto_fixture.html', {'form': AutoFixtureForm})
