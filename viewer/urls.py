"""persons URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.urls import path

from .views import viewer, detail, detail_person_save, detail_person_doc_save



urlpatterns = [
    path('detail_person_doc_save/<int:pk>', detail_person_doc_save, name='detail_person_doc_save'),
    path('detail_person_save/<int:pk>', detail_person_save, name='detail_person_save'),
    path('<int:pk>', detail, name='detail'),
    path('', viewer, name='viewer'),

]
