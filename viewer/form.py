from django.forms import ModelForm, TextInput, Select
from django import forms
from .models import Person, PersonsDocs

class PersonDetailForm(ModelForm):
    """
    Person detail form based on model (Person)
    """
    class Meta:
        model = Person
        fields = [
            'fio', 'birthday_date', 'sex', 'mobile_number', 'period_of_study_from',
            'period_of_study_to', 'group', 'edu_organization'
        ]

        widgets = {
        'fio': TextInput(attrs={'class': "form-control", 'id': "fio"}),
        'birthday_date': TextInput(attrs={'class': "form-control", 'id': "birthday_date"}),
        'sex': Select(attrs={'class': "form-control", 'id': "sex"}),
        'mobile_number': TextInput(attrs={'class': "form-control", 'id': "mobile_number"}),
        'period_of_study_from': TextInput(attrs={'class': "form-control", 'id': "period_of_study_from"}),
        'period_of_study_to': TextInput(attrs={'class': "form-control", 'id': "period_of_study_to"}),
        'group': TextInput(attrs={'class': "form-control", 'id': "group"}),
        'edu_organization': TextInput(attrs={'class': "form-control", 'id': "edu_organization"})
        }


class PersonsDocsForm(ModelForm):
    """
    Person doc detail form based on model (PersonsDocs)
    """
    class Meta:
        model = PersonsDocs
        fields = ['number', 'extradition_date', 'doc_type', 'doc_scan']

        widgets = {
            'number': TextInput(attrs={'class': "form-control", 'id': "number"}),
            'extradition_date': TextInput(attrs={'class': "form-control", 'id': "extradition_date"}),
            'doc_type': Select(attrs={'class': "form-control", 'id': "doc_type"}),
        }


class SearchForm(forms.Form):
    text_search = forms.CharField(required=False, widget=TextInput(attrs={
    'class': "form-control mr-1", 'id': "text_search", 'placeholder': "Текстовый поиск", 'name': "text_search"
    }))
    birthday_search = forms.DateField(required=False, widget=TextInput(attrs={
        'class': "form-control mr-1", 'id': "birthday_search", 'placeholder': "Дата рождения", 'name':"birthday_search"
    }))
    study_from_search = forms.DateField(required=False, widget=TextInput(attrs={
        'class': "form-control mr-1", 'id': "study_from_search", 'placeholder': "Срок обучения с",
            'name': "study_from_search"
    }))
    study_to_search = forms.DateField(required=False, widget=TextInput(attrs={
        'class': "form-control mr-1", 'id': "study_to_search",  "placeholder": "Срок обучения по",
            'name': "study_to_search"
    }))