from .form import SearchForm

def search_form_variable(request):
    return {
        'search_form': SearchForm
    }