from django.contrib import admin

from .models import Person, PersonsDocs
# Register your models here.

admin.site.register(Person)
admin.site.register(PersonsDocs)