from django.shortcuts import render, redirect
from django.core.paginator import Paginator
from django.db.models import Q


from .models import Person, PersonsDocs
from .form import PersonDetailForm, PersonsDocsForm

# Create your views here.


def viewer(request):
    """
    If method == POST, make query based on POST data and filter result.
    else
    Get all people from Person model and their related doc if it's == passport
    paginate it and render.
    :param request:
    :return: render
    """
    if request.method == 'POST':
        query = []
        text = request.POST['text_search']
        if text:
            query.append(Q(fio__contains=text) | Q(sex__contains=text) | Q(mobile_number__contains=text) | Q(
                group__contains=text) | Q(edu_organization__contains=text))

        birthday = request.POST['birthday_search']
        if birthday:
            query.append(Q(birthday_date__exact=birthday))

        study_from_search = request.POST['study_from_search']
        if study_from_search:
            query.append(Q(birthday_date__exact=study_from_search))

        study_to_search = request.POST['study_to_search']
        if study_to_search:
            query.append(Q(period_of_study_to__exact=study_to_search))

        persons_list = Person.objects.filter(*query)
        for person in persons_list:
            person.docs = person.documents.filter(doc_type='паспорт')
        paginator = Paginator(persons_list, 10)
        page = request.GET.get('page')
        persons = paginator.get_page(page)
        return render(request, 'viewer/viewer.html', {'persons': persons})
    else:
        persons_list = Person.objects.all()
        for person in persons_list:
            person.docs = person.documents.filter(doc_type='паспорт')
        paginator = Paginator(persons_list, 10)
        page = request.GET.get('page')
        persons = paginator.get_page(page)
        return render(request, 'viewer/viewer.html', {'persons': persons})


def detail(request, pk):
    """
    Get pk as a param, find instance, bind form with the instance and render it
    :param request:
    :param pk: primary key of Person
    :return: render
    """
    instance = Person.objects.get(pk=pk)
    instance_docs = instance.documents.filter(doc_type='паспорт').first()
    form = PersonDetailForm(instance=instance)
    docs_form = PersonsDocsForm(instance=instance_docs)
    return render(request, 'viewer/person_detail.html', {'form': form, 'form_docs': docs_form, 'pk': pk})


def detail_person_save(request, pk):
    """
    Get pk as a param, find instance, bind form with the instance,
    check if form is valid, save it and redirect back to detail.
    :param request:
    :param pk: primary key of Person
    :return: render
    """
    person = Person.objects.get(pk=pk)
    if request.method == 'POST':
        form = PersonDetailForm(request.POST, instance=person)
        if form.is_valid():
            form.save()
            return redirect('detail', pk=pk)

        else:
            return redirect('detail', pk=pk)


def detail_person_doc_save(request, pk):
    """
    Get pk as a param, find instance, then geat a passtort doc,
    bind form with the doc instance, check if form is valid,
    save it and redirect back to detail.
    :param request:
    :param pk: primary key of Person
    :return: render
    """
    person = Person.objects.get(pk=pk)
    document = person.documents.filter(doc_type='паспорт').first()
    if request.method == 'POST':
        form = PersonsDocsForm(request.POST, instance=document)
        if form.is_valid():
            form.save()
            return redirect('detail', pk=pk)

        else:
            return redirect('detail', pk=pk)
