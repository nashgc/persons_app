from django.db import models

# Create your models here.


class Person(models.Model):
    # Person model

    sex_choises = (
        ('муж', 'муж'),
        ('жен', 'жен')
    )

    fio = models.CharField(max_length=255)
    birthday_date = models.DateField()
    sex = models.CharField(max_length=255, choices=sex_choises)
    mobile_number = models.CharField(max_length=255, unique=True)
    period_of_study_from = models.DateField()
    period_of_study_to = models.DateField()
    group = models.CharField(max_length=255)
    edu_organization = models.CharField(max_length=255)

    def __str__(self):
        return self.fio

    class Meta:
        verbose_name_plural = 'Персоны'


class PersonsDocs(models.Model):
    # Doc model

    doc_type_choises = (
        ('паспорт', 'паспорт'),
        ('студенческий', 'студенческий'),
        ('св. о рождении', 'св. о рождении'),
        ('права', 'права')
    )

    number = models.CharField(max_length=255, unique=True)
    extradition_date = models.DateField()
    doc_type = models.CharField(max_length=255, choices=doc_type_choises)
    doc_scan = models.FileField(upload_to='doc_scan/')
    person = models.ForeignKey(Person, on_delete=models.CASCADE, related_name='documents')

    def __str__(self):
        return self.number

    class Meta:
        verbose_name_plural = 'Документы'
